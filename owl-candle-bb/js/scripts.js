
  // define global func for jsonp callback
  // var ringSize = null;
  //
  window.addVariantToCart = function(product_id_json) {

    // https://www.fragrantjewels.com/pages/add-to-cart?v=8161774339

    // check for ios devices
    var addToCartUrl = "https://www.fragrantjewels.com/pages/add-to-cart?v=" + product_id_json[ringSize];

    if (window.navigator.userAgent.indexOf("Safari") > -1) {
      document.location = addToCartUrl;
    } else {
      window.location = addToCartUrl;
    }
  };

  $(document).ready(function() {
    var lb,
      lightTimeout;

      //preloader
      function startLoader() {
        lb = new $.LoadingBox({
          mainElementID: 'loading-box',
          fadeInSpeed: 'normal',
          fadeOutSpeed: 'normal',
          opacity: 0.7,
          backgroundColor: "#000",
          loadingImageSrc: "img/loader.gif"
        });
      }

      //productModule
      var productModule = (function() {
        var productData = {};

        var getProductData = function() {
          return productData;
        }

        var setProductData = function(data) {
          productData = data;
        }

        return {
          getProductData: getProductData,
          setProductData: setProductData
        }
      }());

      function prepareToCart (elem, product_id_json) {
        var $ringSelect = $(elem).find('.ring-size-select'),
          ringSize = elem.prop('value'),
          productData = productModule.getProductData();

          if(productData.id){
            $.get("https://fjrecurly.herokuapp.com/shopify_endpoint/add_variant_to_cart?callback=addVariantToCart&product_id=" + productData.id)
            startLoader();
          } else {
            console.error("Check product ID");
          }


        // https://www.fragrantjewels.com/pages/add-to-cart?v=8161774339
        // console.log(product_id_json);
        // check for ios devices
        // var addToCartUrl = "https://www.fragrantjewels.com/pages/add-to-cart?v=" + product_id_json[ringSize];
        // console.log(addToCartUrl);

        // if (window.navigator.userAgent.indexOf("Safari") > -1) {
        //   document.location = productData;
        // } else {
        //   window.location = addToCartUrl;
        // }



        // window.location = productData.link || "https://www.fragrantjewels.com";
      };

      //ringSizeDataModule
      var ringSizeDataModule = (function() {
        var ringData = {};
        var getProductData = function(productId) {
          return $.ajax({
            url: "https://fjrecurly.herokuapp.com/shopify_endpoint/add_variant_to_cart?product_id=" + productId,
            dataType: "jsonp"
          });
        }

        var getRingData = function() {
          return ringData;
        }

        var setRingData = function(data) {
          ringData = data;
        }

        return {
          getRingData: getRingData,
          setRingData: setRingData,
          getProductData: getProductData
        };

      })();

      var createSelect = function(elem, ringData) {
        var $selectHolder = $(elem),
          sizeNode;

        if ($selectHolder.length && ringData) {

          for (var i in ringData) {
            if (ringData.hasOwnProperty(i)) {
              sizeNode = '<button type="button" value="' + i + '">' + i + '</button>';
              if (i.toLowerCase() == "surprise me!") {
                $selectHolder.prepend(sizeNode);
              } else {
                $selectHolder.append(sizeNode);
              }
            }
          }

          // var selectNode = '<div class="select-style">' +
          //   '<select class="ring-size-select">' +
          //   '<option value="0" disabled="" selected="selected">SELECT RING SIZE</option>' +
          //   '</select>' +
          //   '</div>';
          //
          // $selectHolder.append(selectNode);
          //
          // $ringSelect = $selectHolder.find(".ring-size-select");
          // for (var i in ringData) {
          //   if (ringData.hasOwnProperty(i)) {
          //     optionNode = '<option value="' + i + '">' + i + '</option>';
          //     if (i.toLowerCase() == "surprise me!") {
          //       $(optionNode).insertAfter($selectHolder.find('.ring-size-select option:first-child'));
          //     } else {
          //       $ringSelect.append(optionNode);
          //     }
          //   }
          // }

          $selectHolder.on("click", "button", function() {
            if($(this).prop('value')){
              var ringData = ringSizeDataModule.getRingData();
              prepareToCart($(this), ringData);
            }
          });
        }
      }


    // Modal
    // $('#wickedModal').modal({
    //   // backdrop: 'static',
    //   show: true
    // });

    // init image slider
    // var mySwiper = new Swiper('.swiper-container', {
    //   // Optional parameters
    //   direction: 'horizontal',
    //   loop: true,
    //   // If we need pagination
    //   pagination: '.swiper-pagination',
    //   // Navigation arrows
    //   nextButton: '.swiper-button-next',
    //   prevButton: '.swiper-button-prev',
    //   // And if we need scrollbar
    //   // scrollbar: '.swiper-scrollbar',
    // autoplay: 3000,
    // })

    // Countdown
    // function initCounter() {
    //   var currentTime = new Date().getTime(),
    //     remainingTime,
    //     startTimer,
    //     display = $('#countdown'),
    //     fullSeconds = 3598;
    //
    //   if (!Cookies.get('promo-expiration')) {
    //     try {
    //       Cookies.set('promo-expiration', currentTime, { expires: 1, path: '/' });
    //     } catch (e) {
    //       console.log("Error");
    //     }
    //   }
    //
    //   startTimer = Cookies.get('promo-expiration');
    //   remainingTime = fullSeconds - ((currentTime - startTimer) / 1000).toFixed(0);
    //   remaining = (startTimer + fullSeconds);
    //   if (remainingTime <= 0) {
    //     remainingTime = 0;
    //     giftExpired();
    //   }
    //   if (display.length > 0) {
    //     setTimer(remainingTime, display);
    //   }
    // }
    //
    // function setTimer(duration, display) {
    //   var minutes, seconds;
    //   var inter = setInterval(function() {
    //     minutes = parseInt(duration / 60, 10);
    //     seconds = parseInt(duration % 60, 10);
    //     minutes = (minutes < 10) ? ("0" + minutes) : minutes;
    //     seconds = (seconds < 10) ? ("0" + seconds) : seconds;
    //     display.html("<span class='c-timer c-hour'>00</span><span class='c-timer c-min'>" + minutes + "</span><span class='c-timer c-sec'>" + seconds + "</span>");
    //     if (--duration < 0) {
    //       giftExpired();
    //       clearInterval(inter);
    //     }
    //   }, 1000);
    // }
    //
    // initCounter();

    // function giftExpired() {
    //   Cookies.remove('promo-expiration', { path: "/" });
    //   try {
    //     initCounter();
    //   } catch (e) {
    //     console.log("Error");
    //   }
    // }

    //select
    // $('.wicked-select').selectpicker();
    // $('.ring-size-submit').on('click', function() {
    //   var $selectNode = $(this).closest('form').find('select'),
    //     $selectBtnNode = $(this).closest('form').find('.dropdown-toggle'),
    //     lightTimeout;
    //   ringSize = $selectNode.prop('value');
    //   if (!ringSize) {
    //     clearTimeout(lightTimeout);
    //     $selectBtnNode.css({ 'background': 'RGBA(100,74,37,1.00)', 'color': 'white' });
    //     lightTimeout = setTimeout(function() {
    //       $selectBtnNode.css({ 'background': 'white', 'color': 'RGBA(208,168,74,1.00)' });
    //     }, 400);
    //   } else {
    //     // action
    //     // calling shopify page to add variant to cart
    //     console.log("ringSize", ringSize);
    //     var inner_circle_product_id = 1413242683470;
    //     // Using JSONP
    //     $.get("https://fjrecurly.herokuapp.com/shopify_endpoint/add_variant_to_cart?callback=addVariantToCart&product_id=" + inner_circle_product_id)
    //   }
    // });
    // //Reserve btn
    // $('#reserve-btn').on('click', function() {
    //   //action
    //   $('#wickedModal').modal('hide');
    // });
  // });


    function debounce(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    };

    var windowScrollToSliderHandler = debounce(function() {
      var $slider = $(".slick-wrapper");
      if($slider.length){
        if($(window).scrollTop() + $(window).height() >= $slider.offset().top + ($slider.height() - $slider.height()/2)){
          $slider.find('.slick-list').removeClass('slick-list-active');
          $(window).off('scroll', windowScrollToSliderHandler);
        }
      } else {
        $slider.find('.slick-list').removeClass('slick-list-active');
        $(window).off('scroll', windowScrollToSliderHandler);
      }
    }, 20);

    var windowScrollToSliderHandler2 = debounce(function() {
      var $slider = $(".slick-wrapper-2");
      if($slider.length){
        if($(window).scrollTop() + $(window).height() >= $slider.offset().top + ($slider.height() - $slider.height()/2)){
          $slider.find('.slick-list').removeClass('slick-list-active');
          $(window).off('scroll', windowScrollToSliderHandler2);
        }
      } else {
        $slider.find('.slick-list').removeClass('slick-list-active');
        $(window).off('scroll', windowScrollToSliderHandler2);
      }
    }, 20);

    var windowScrollToSliderHandler3 = debounce(function() {
      var $slider = $(".slick-wrapper-3");
      if($slider.length){
        if($(window).scrollTop() + $(window).height() >= $slider.offset().top + ($slider.height() - $slider.height()/2)){
          $slider.find('.slick-list').removeClass('slick-list-active');
          $(window).off('scroll', windowScrollToSliderHandler3);
        }
      } else {
        $slider.find('.slick-list').removeClass('slick-list-active');
        $(window).off('scroll', windowScrollToSliderHandler3);
      }
    }, 20);

    $('.slick-wrapper').on('init', function(event, slick, direction){
      $(window).on('scroll', windowScrollToSliderHandler);
      $(this).find('.slick-list').addClass("slick-list-active");
      windowScrollToSliderHandler();
    });
    $('.slick-wrapper-2').on('init', function(event, slick, direction){
      $(window).on('scroll', windowScrollToSliderHandler2);
      $(this).find('.slick-list').addClass("slick-list-active");
      windowScrollToSliderHandler2();
    });
    $('.slick-wrapper-3').on('init', function(event, slick, direction){
      $(window).on('scroll', windowScrollToSliderHandler3);
      $(this).find('.slick-list').addClass("slick-list-active");
      windowScrollToSliderHandler3();
    });
    $('.slick-wrapper').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      adaptiveHeight: true,
      lazyLoad: 'ondemand'
    });
    $('.slick-wrapper-2').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      adaptiveHeight: true,
      lazyLoad: 'ondemand'
    });
    $('.slick-wrapper-3').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      adaptiveHeight: true,
      lazyLoad: 'ondemand'
    });

    $(".ic-section__ol-list-accordion").on("click", ".ic-section__ol-list__heading", function(){
      var $text = $(this).closest("li").find(".ic-section__ol-list__text");
      if($text.is(':hidden')){
        $text.slideDown(300);
        $(this).addClass('active-item');
      } else {
        $text.slideUp(300);
        $(this).removeClass('active-item');
      }
    });


    function windowScrollHandler() {
      if($("#join-btn-anchor").length){
        if($(window).scrollTop() + $(window).height() >= $("#join-btn-anchor").offset().top + $("#join-btn-anchor").height()){
          $("#join-btn-wrapper-fixed").show(0);
          $(window).off('scroll', windowScrollHandler);
        }
      } else {
        $("#join-btn-wrapper-fixed").show(0);
        $(window).off('scroll', windowScrollHandler);
      }
    };



    windowScrollHandler();
    $(window).on('scroll', windowScrollHandler);

    function showElement(elem) {
      $(elem).animate({
        opacity: 1
      }, 1000);
    }

    function addButtonEvent(elem){
      if(elem){
        $(elem).on('click', function(){
          $('#ring-size-modal').modal({
            show: true
          })
        });
      }
    }

    function init() {
      startLoader();
        $.get("./api/data.json", function(data) {
          if(data && data.type){
            // var product = shuffle(data)[0];
            productModule.setProductData(data);
          } else {
            console.error('Unknown product information.');
          }
        }).done(function() {
          var productData = productModule.getProductData();

          if (productData && productData.type) {
            switch (productData.type) {
              case 'ring':
                ringSizeDataModule.getProductData(productData.id).done(function(ringData) {
                  ringSizeDataModule.setRingData(ringData);
                  createSelect('.select-holder', ringData);
                  showElement('.wrapper');
                  addButtonEvent('#ic-join-btn');
                  lb.close();
                }).error(function(e) {
                  console.error(e);
                });
                break;
              default:
                console.error('Unknown product type.');
            }
          } else {
            console.error('Unknown product information.');
          }
        });

    }

    init();

  });
