$(document).ready(function() {

$(".sidebarNavigation .navbar-nav-outer").hide(0).clone().appendTo("body").removeAttr("class").addClass("sideMenu").show().find(".navbar-nav").removeAttr("class").addClass("side-menu");
$(".sidebarNavigation .navbar-nav-outer").show(0);
  $("body").append("<div class='overlay-n'></div>");
$(".overlay-n, .sideMenu").css("top", $(".site-header").offset().top + $(".site-header").outerHeight())

$(".navbar-toggler").on("click", function() {
    checkMobileMenuMargin();
  $("body").toggleClass("overlay-open");
  $(".sideMenu").addClass($(".sidebarNavigation").attr("data-sidebarClass"));
  $(".sideMenu, .overlay-n").toggleClass("open");
  $(".overlay-n").on("click", function() {
    $(this).removeClass("open");
    navbarArrowClickHandler();
    $(".sideMenu").removeClass("open");
    $("body").removeClass("overlay-open");
  })
});
$("body").on("click", ".sideMenu.open>li", function() {
  if (!$(this).hasClass("site-nav--has-dropdown")) {
    $(".sideMenu, .overlay-n").toggleClass("open");
    $("body").removeClass("overlay-open");
  }
});
  function checkMobileMenuMargin(){
  $(".overlay-n, .sideMenu").css("top", $(".site-header").offset().top + $(".site-header").outerHeight() - $(window).scrollTop() )
  }
    $(".site-nav--has-dropdown").on("click", function(){
      var $navbarNav2 = $(".navbar-nav-2"),
          heading = $($(this).find("[data-activator]").get(0)).text() || "";

      $navbarNav2.find(".navbar-nav-2__content").empty();
      if($(window).width() <= 600){
        $(this).find(".fj-dropdown").clone().appendTo(".navbar-nav-2__content").removeAttr("class").addClass("fj-dropdown-n");
        $('<li class="item-divider"></li>').prependTo(".fj-dropdown-n");
        $('<li class="item-name"></li>').prependTo(".fj-dropdown-n").text(heading);
        $('<li class="item-divider"></li>').appendTo(".fj-dropdown-n");
        $(".side-menu").find(".mobile-login-links").each(function(index, elem) {
          console.log("index", index);
          $(elem).clone().appendTo(".fj-dropdown-n");
        });
        headerTogglerChange(true, $(".navbar-toggler"), $(".navbar-back"));
        $(".side-menu").fadeOut(400, function(){
          $(".sideMenu").find(".top-navbar-nav-holder").hide(0);
          $navbarNav2.fadeIn(400);
        });
      }
    });

  function headerTogglerChange(flag, $elem1, $elem2){
    if($elem1 && $elem2){
      if(flag){
        $elem1.fadeOut(400);
        $elem2.fadeIn(400);
      } else {
        $elem1.fadeIn(400);
        $elem2.fadeOut(400);
      }
    }
  }

  $(".navbar-back").on("click", navbarArrowClickHandler);

  function navbarArrowClickHandler(){
    headerTogglerChange(false, $(".navbar-toggler"), $(".navbar-back"));
      $(".navbar-nav-2").fadeOut(400, function(){
          $(".sideMenu").find(".top-navbar-nav-holder").show(0);
          $(".side-menu").fadeIn(400);
        })
  }

  $(window).resize(function() {
    $(".navbar-back").hide(0);
    $(".navbar-nav-2").hide(0);
    $(".side-menu").show(0);
    $(".sideMenu").find(".top-navbar-nav-holder").show(0);
    $(".sideMenu, .overlay-n").removeClass("open");
    $("body").removeClass("overlay-open");
    $(".navbar-toggler").show(0);
      });
});
