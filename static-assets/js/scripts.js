// define global func for jsonp callback
var ringSize = null;

window.addVariantToCart = function(product_id_json) {
  // https://www.fragrantjewels.com/pages/add-to-cart?v=8161774339
  console.log(product_id_json);
  // check for ios devices
  var addToCartUrl = "https://www.fragrantjewels.com/pages/add-to-cart?v=" + product_id_json[ringSize];
  console.log(addToCartUrl);
  if (window.navigator.userAgent.indexOf("Safari") > -1) {
    document.location = addToCartUrl;
  } else {
    window.location = addToCartUrl;
  }
};

$(document).ready(function() {
  // Modal
  $('#wickedModal').modal({
    // backdrop: 'static',
    show: true
  });

  // init image slider
  var mySwiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    // If we need pagination
    pagination: '.swiper-pagination',
    // Navigation arrows
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    // And if we need scrollbar
    // scrollbar: '.swiper-scrollbar',
	autoplay: 3000,
  })

  // Countdown
  function initCounter() {
    var currentTime = new Date().getTime(),
      remainingTime,
      startTimer,
      display = $('#countdown'),
      fullSeconds = 3598;

    if (!Cookies.get('promo-expiration')) {
      try {
        // Cookies.set('promo-expiration', currentTime, { expires: 1, path: '/' , domain:'fragrantjewels.com'});
        Cookies.set('promo-expiration', currentTime, { expires: 1, path: '/' });
      } catch (e) {
        console.log("Error");
      }
    }

    startTimer = Cookies.get('promo-expiration');
    remainingTime = fullSeconds - ((currentTime - startTimer) / 1000).toFixed(0);
    remaining = (startTimer + fullSeconds);
    if (remainingTime <= 0) {
      remainingTime = 0;
      giftExpired();
      // return false;
    }
    if (display.length > 0) {
      // $('#countdown').timeTo({
      //   seconds: remainingTime,
      //   fontSize: 36,
      //   callback: function () {
      //     giftExpired();
      //   }
      // });
      setTimer(remainingTime, display);
    }
  }

  function setTimer(duration, display) {
    var minutes, seconds;
    var inter = setInterval(function() {
      minutes = parseInt(duration / 60, 10);
      seconds = parseInt(duration % 60, 10);
      minutes = (minutes < 10) ? ("0" + minutes) : minutes;
      seconds = (seconds < 10) ? ("0" + seconds) : seconds;
      display.html("<span class='c-timer c-hour'>00</span><span class='c-timer c-min'>" + minutes + "</span><span class='c-timer c-sec'>" + seconds + "</span>");
      if (--duration < 0) {
        giftExpired();
        clearInterval(inter);
      }
    }, 1000);
  }

  initCounter();

  function giftExpired() {
    Cookies.remove('promo-expiration', { path: "/" });
    try {
      initCounter();
    } catch (e) {
      console.log("Error");
    }
  }

  //select
  $('.wicked-select').selectpicker();
  $('.ring-size-submit').on('click', function() {
    var $selectNode = $(this).closest('form').find('select'),
      $selectBtnNode = $(this).closest('form').find('.dropdown-toggle'),
      lightTimeout;
    ringSize = $selectNode.prop('value');
    if (!ringSize) {
      clearTimeout(lightTimeout);
      $selectBtnNode.css({ 'background': 'RGBA(100,74,37,1.00)', 'color': 'white' });
      lightTimeout = setTimeout(function() {
        $selectBtnNode.css({ 'background': 'white', 'color': 'RGBA(208,168,74,1.00)' });
      }, 400);
    } else {
      // action
      // calling shopify page to add variant to cart
      console.log("ringSize", ringSize);
      var inner_circle_product_id = 198087311384;
      // Using JSONP
      $.get("https://fjrecurly.herokuapp.com/shopify_endpoint/add_variant_to_cart?callback=addVariantToCart&product_id=" + inner_circle_product_id)
    }
  });
  //Reserve btn
  $('#reserve-btn').on('click', function() {
    //action
    $('#wickedModal').modal('hide');
  });
});
